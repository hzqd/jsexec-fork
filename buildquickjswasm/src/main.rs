use std::{io::ErrorKind, ffi::CString, env::args_os};
use std::os::raw::c_char;

extern "C" {
    fn runjs(ch:*const c_char,len:usize);
}

const INPUT_ERROR : ErrorKind = ErrorKind::InvalidInput;
fn main() ->Result<(),ErrorKind> {
    let mut args = args_os();
    args.next().ok_or(INPUT_ERROR)?;
    let s = args.next().ok_or(INPUT_ERROR)?.to_string_lossy().to_string();
    let len = s.len();
    let sc = CString::new(s).map_err(|_|INPUT_ERROR)?;
    
    unsafe {
        runjs(sc.as_ptr(), len);
    }
    Ok(())
}
