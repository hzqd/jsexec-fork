#[doc = include_str!("../README.md")]
use std::{error::Error, io::Read};

use wasmer::*;
use wasmer_wasi::{Pipe, WasiState};

pub fn runjs(source: &str) -> Result<String, Box<dyn Error>> {
    let engine = EngineBuilder::headless();
    let mut store = Store::new(engine);
    let module = unsafe {
        Module::deserialize(&store, include_bytes!(concat!(env!("OUT_DIR"), "/qjs.lib")))
            .expect("failed to load module")
    };
    let mut output = Pipe::new();
    let wasi_env = WasiState::new("qjs")
        .arg(source)
        .stdout(Box::new(output.clone()))
        .finalize(&mut store)?;

    let import_object = wasi_env.import_object(&mut store, &module)?;
    let instance = Instance::new(&mut store, &module, &import_object)?;

    let memory = instance.exports.get_memory("memory")?;
    wasi_env.data_mut(&mut store).set_memory(memory.clone());

    let start = instance.exports.get_function("_start")?;
    start.call(&mut store, &[])?;

    let mut buf = String::new();
    output.read_to_string(&mut buf)?;
    Ok(buf)
}

#[test]
fn test() {
    let source = std::fs::read_to_string("hello.js").unwrap();
    assert_eq!(runjs(&source).unwrap(), "hello\n");
}
